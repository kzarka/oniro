# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0
#
# Utils functions for upgrade_oniro.py


from curses.ascii import isdigit
from heapq import merge
from locale import atoi
from pathlib import Path
import os
import shutil


def get_manifest_projects(oniro_path):
    """
    Create a list of dict containing all the properties of the
    projects in Oniro's manifest.

    Args:
        oniro_path: Path to Oniro project, which contains the oniro folder and other meta- folders
    Returns:
        A new list of dict with all the projects' properties.
    """
    import xml.etree.ElementTree as ET

    tree = ET.parse(f"{oniro_path}/oniro/manifests/default.xml")
    root = tree.getroot()
    return [project.attrib for project in root.findall("project")]


def find_latest_backup(base_dir):
    """
    Find the latest backup of Oniro. This will search only in the
    current directory.

    Args:
        base_dir: folder where all the backups are stored

    Returns:
        The latest backup name if found, None otherwise.
    """

    this_dir = Path(base_dir)
    backup_info_list = []
    for item in this_dir.iterdir():
        if "upgrade_oniro_backup" in item.name:

            backup_info = {
                "name": item.name,
                "timestamp": item.name.split("-")[1].split(".")[:2],
            }
            backup_info_list.append(backup_info)

    return (
        None
        if backup_info_list == []
        else "%s/%s"
        % (
            base_dir,
            max(
                backup_info_list,
                key=lambda x: int(f'{x["timestamp"][0]}{x["timestamp"][1]}'),
            )["name"],
        )
    )


def get_layers(bbl_file):
    """
    Get the layers names from a specified bblayers file.

    Args:
        bbl_file: bblayers conf file where the layers will be extracted from

    Returns:
        A set of strings corresponding to the layers in the specified bblayers file and the content
        of the file itself.
    """
    f_content = ""
    with open(bbl_file) as f:
        f_content = f.readlines()

    f_content = [x.strip() for x in f_content]
    i = 0
    while i < len(f_content):
        item = f_content[i]
        if len(item) > 0 and item[-1] == "\\":
            # merge this with the next, deleting the `\`
            to_merge = f_content[i + 1]
            f_content.remove(to_merge)
            f_content[i] = item[:-1] + to_merge

        else:
            i += 1

    # Extract BBLAYERS
    bbl_str = ""
    for item in f_content:
        if item[:8] == "BBLAYERS":
            bbl_str = item

    # Extract content
    bbl_str = bbl_str.split('"')[1].strip()

    # Get the layers
    layers_path = bbl_str.split(" ")
    return set([(l.split("/")[-1], l) for l in layers_path]), f_content


def create_bblayers(merged_layers, bbl_content, build_dir):
    """
    Write the specified layers in the bblayers.conf file in the
    build directory.

    Args:
        merged_layers: layers to be written
        bbl_content: content of a bblayers.conf file
        build_dir: build directory of the project.

    Returns:
        A set of strings corresponding to the layers in the specified bblayers file.
    """
    final_bblayer_str = 'BBLAYERS ?= " \\\n'
    for layer in merged_layers:
        final_bblayer_str += f"    {layer} \\\n"
    final_bblayer_str += '"'

    for i in range(0, len(bbl_content)):
        if bbl_content[i][:8] == "BBLAYERS":
            bbl_content[i] = final_bblayer_str
            break

    with open(build_dir / "conf/bblayers.conf", "w") as f:
        f.writelines([l + "\n" for l in bbl_content])
    return


def update_oniro_inc(oniro_path):
    """
    Update the ONIRO_LAYERS_CONF_VERSION variable value if necessary,
    that is when ONIRO_REQUIRED_LAYERS_CONF_VERSION has a different
    value.

    Args:
        oniro_path: Path to Oniro project, which contains the oniro folder and other meta- folders

    Returns:
        0 if the oniro.inc file has not been found
        1 if the above mentioned variables have not been found
        2 if the update has been successful
    """
    import re

    file_name = f"{oniro_path}/oniro/meta-oniro-core/conf/distro/include/oniro.inc"
    if os.path.isfile(file_name) == False:
        return 0
    found_oniro_required_layer_conf_version = False
    found_oniro_layer_conf_version = False
    oniro_required_layer_conf_version = []
    oniro_layer_conf_version = []
    file_data = []
    new_file_data = []
    with open(file_name, "r") as f:
        file_data = f.readlines()
    for line in file_data:
        if line.startswith("ONIRO_REQUIRED_LAYERS_CONF_VERSION"):
            found_oniro_required_layer_conf_version = True
            oniro_required_layer_conf_version = re.findall(
                r'"(.+?)"', line.split("=")[-1]
            )
        elif line.startswith("ONIRO_LAYERS_CONF_VERSION"):
            found_oniro_layer_conf_version = True
            oniro_layer_conf_version = re.findall(r'"(.+?)"', line.split("=")[-1])
    if (
        found_oniro_layer_conf_version == False
        or found_oniro_required_layer_conf_version == False
    ):
        return 1
    for value in oniro_layer_conf_version:
        oniro_layer_conf_version = str(value)
    for value in oniro_required_layer_conf_version:
        oniro_required_layer_conf_version = str(value)

    for line in file_data:
        if line.startswith("ONIRO_LAYERS_CONF_VERSION"):
            new_file_data.append(
                line.split("=")[0]
                + "= "
                + '''"'''
                + f"{oniro_required_layer_conf_version}"
                + '''"'''
                + "\n"
            )
        else:
            new_file_data.append(line)
    with open(file_name, "w") as f:
        f.writelines(new_file_data)
    return 2


def update_bblayers(build_dir, flavour, old_layers, oniro_path):
    """
    Use the sample file in Oniro and the current in the build
    directory to create a new file containing the new layers
    of Oniro and keeping the custom ones of the user.

    Args:
        build_dir: the path of the build directory
        old_layers: list returned by get_layers of the old sample
                    file (before the oniro repo upgrade)
        oniro_path: Path to Oniro project, which contains the oniro folder and other meta- folders

    Returns:
        Nothing, bblayers.conf in the build directory will be
        updated.
    """
    import os

    target_layers, bbl_content = get_layers(Path(build_dir / "conf/bblayers.conf"))
    target_layers_names = set([l[0] for l in target_layers])
    new_layers, _ = get_layers(
        Path(f"{oniro_path}/oniro/flavours/{flavour}/bblayers.conf.sample")
    )
    new_layers_names = set([l[0] for l in list(new_layers)])
    old_layers_names = set([l[0] for l in list(old_layers)])

    user_layers_name = target_layers_names - old_layers_names

    # Be sure that duplicate layers are not included twice
    final_layers_names = list(set(list(new_layers_names) + list(user_layers_name)))

    # Get the layer paths
    oe_dir = f"{oniro_path}/oe-core"
    merged_bbl_layers = []
    for l in final_layers_names:
        found = False
        for nl in list(new_layers):
            if nl[0] == l:
                # The layer is in here, get the path and create the final path
                merged_bbl_layers.append(nl[1].replace("##OEROOT##", str(oe_dir)))
                found = True

        if not found:
            # It's a user layer
            for ul in list(target_layers):
                if ul[0] == l:
                    # The layer is in here, get the path and create the final path
                    merged_bbl_layers.append(ul[1])

    merged_bbl_layers = list(set(merged_bbl_layers))
    merged_bbl_layers.sort()
    create_bblayers(merged_bbl_layers, bbl_content, build_dir)


def update_local_conf(build_dir, local_conf_diff, flavour, oniro_path):
    """
    Update the local.conf file in the build directory. The function
    uses the differences between the old (pre-update) local.conf and
    local.conf.sample files and applies those to the new local.conf.sample,
    making the new local.conf in the build directory of the project.

    Args:
        build_dir: the path of the build directory
        local_conf_diff = list of strings with differences between old local.conf and local.conf.sample,
        where each string has a '+' as first character if it should be added in the new local.conf
        or '-' if it should be removed (if it exists in the new sample).
        flavour: flavour of Oniro (linux, freertos, zephys,...)
        oniro_path: Path to Oniro project, which contains the oniro folder and other meta- folders

    Returns:
        Nothing, local.conf in the build directory will be
        updated.
    """
    os.remove(f"{build_dir}/conf/local.conf")
    shutil.copy(
        f"{oniro_path}/oniro/flavours/{flavour}/local.conf.sample", f"{build_dir}/conf"
    )
    os.rename(f"{build_dir}/conf/local.conf.sample", f"{build_dir}/conf/local.conf")

    with open(f"{build_dir}/conf/local.conf", "r") as f:
        conf_data = f.readlines()

    for lines in local_conf_diff:
        if lines.startswith("-"):
            if lines[1:] in conf_data:
                conf_data.remove(lines[1:])
        else:
            if lines[1:] not in conf_data:
                conf_data.append(lines[1:])

    write_data = []
    for line in conf_data:
        write_data.append(line.replace("\n", ""))
    write_data = "\n".join(write_data)

    with open(f"{build_dir}/conf/local.conf", "w") as f:
        f.write(write_data)


def get_layers_paths(bbl_file):
    # This function will return the layers specified in the bbl_file provided
    # It will return the full layer path, for example meta-openembedded/meta-python
    # instead of only meta-python
    # This will be useful when updating .bbappend files in custom layers,
    # by reducing iterations only to needed files/folders.
    """
    Get the path of the layers from a specified bblayers conf file.

    Args:
        bb_file: a bblayers conf file.

    Returns:
        The paths of the layers in the bblayers conf file.
    """
    f_content = ""
    with open(bbl_file, "r") as f:
        f_content = f.readlines()

    f_content = [x.strip() for x in f_content]
    i = 0
    while i < len(f_content):
        item = f_content[i]
        if len(item) > 0 and item[-1] == "\\":
            # merge this with the next, deleting the `\`
            to_merge = f_content[i + 1]
            f_content.remove(to_merge)
            f_content[i] = item[:-1] + to_merge

        else:
            i += 1

    # Extract BBLAYERS
    bbl_str = ""
    for item in f_content:
        if item[:8] == "BBLAYERS":
            bbl_str = item

    # Extract content
    bbl_str = bbl_str.split('"')[1].strip()
    # Get the layers
    layers_path_raw = bbl_str.split(" ")
    layers_path_clean = []

    for layer in layers_path_raw:
        if "/../" in layer:
            layers_path_clean.append(layer.split("/../")[-1])
        else:
            layers_path_clean.append(layer.split("/")[-1])
    return set(layers_path_clean)


def update_bbappends(build_dir, flavour, oniro_path):
    # This function updates the .bbappend files in the custom layers
    # If there are more versions of a recipe, the user will have the
    # choice to apply a .bbappend to all of them, by replacing the
    # version name with _% suffix
    """
    Update the .bbappend files names in the custom layers.
    If there are multiple versions of a recipe in the sample layers, the user will have the
    choice to apply a .bbappend to all of them by replacing the version name with a _% suffix.
    Otherwise, .bbappend files names will be updated with the latest version of the corresponding
    .bb recipe.

    Args:
      build_dir: build directory of the project
      flavour: flavour of Oniro (linux, freertos, zephys,...)
      oniro_path: Path to Oniro project, which contains the oniro folder and other meta- folders

    Returns:
      A list of strings consisting of the .bbappend files updates.
    """
    updated_files = []
    recipes_version = {}
    multiple_versions_recipes = set()
    target_layers_names = get_layers_paths(f"{build_dir}/conf/bblayers.conf")
    old_layers_names = get_layers_paths(
        f"{oniro_path}/oniro/flavours/{flavour}/bblayers.conf.sample"
    )
    user_layers_names = list(target_layers_names - old_layers_names)

    for layer in old_layers_names:
        pathlist = Path(layer).glob("**/*.bb")
        for filename in pathlist:
            filename = str(filename)
            filename = filename.split("/")[-1]

            filename = filename[:-3]
            if "_" in filename:
                filename_list = filename.split("_")
                if len(filename_list) == 2:
                    if filename_list[0] in recipes_version:
                        multiple_versions_recipes.add(filename_list[0])
                    else:
                        recipes_version[filename_list[0]] = filename_list[1]

    for layer in user_layers_names:
        pathlist = Path(layer).glob("**/*.bbappend")
        for full_filename_path in pathlist:
            full_filename_path = str(full_filename_path)
            full_filename = full_filename_path.split("/")[-1]
            filename = full_filename[:-9]
            if "_" in filename:
                filename_list = filename.split("_")
                if len(filename_list) == 2:
                    if filename_list[0] in recipes_version and filename_list[1] != "%":
                        if filename_list[0] in multiple_versions_recipes:
                            print(
                                f"WARNING: {full_filename} cannot be updated automatically. Do you want to apply this .bbappend file to all versions of the related package? [y/n]"
                            )
                            user_choice = input()
                            user_choice = user_choice.lower()
                            if user_choice == "y":
                                new_file_name = filename_list[0] + "_%" + ".bbappend"
                                os.rename(
                                    full_filename_path,
                                    full_filename_path[: -len(full_filename)]
                                    + new_file_name,
                                )
                                updated_files.append(
                                    f"{full_filename} has been renamed to {new_file_name}"
                                )

                        else:
                            # rename file with last available version
                            new_file_name_unique = (
                                filename_list[0]
                                + "_"
                                + recipes_version[filename_list[0]]
                                + ".bbappend"
                            )
                            os.rename(
                                full_filename_path,
                                full_filename_path[: -len(full_filename)]
                                + new_file_name_unique,
                            )
                            updated_files.append(
                                f"{full_filename} has been renamed to {new_file_name_unique}"
                            )

    return updated_files
